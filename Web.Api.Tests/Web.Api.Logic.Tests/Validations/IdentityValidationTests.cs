using FluentAssertions;
using System;
using Web.Api.Logic.Validations;
using Xunit;
// ReSharper disable StringLiteralTypo

namespace Web.Api.Tests.Web.Api.Logic.Tests.Validations
{
    public class IdentityValidationTests
    {
        private readonly IdentityValidation _identityValidation;

        // Arrange
        public IdentityValidationTests()
        {
            _identityValidation = new IdentityValidation();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void Validate_EmptyOrWhitespaceIdNumbers_ShouldThrow(string idNumber)
        {
            // Act
            Action act = () => _identityValidation.Validate(idNumber);

            // Assert
            act.Should().Throw<Exception>().WithMessage("Please provide an ID Number to validate.");
        }

        [Theory]
        [InlineData("9501135803082")]
        [InlineData("9135273082")]
        [InlineData("asdasd")]
        [InlineData("asdasd123")]
        [InlineData("!@#$%")]
        public void Validate_InvalidIdNumbers_ShouldThrow(string idNumber)
        {
            // Act
            Action act = () => _identityValidation.Validate(idNumber);

            // Assert
            act.Should().Throw<Exception>().WithMessage("The provided ID Number is invalid.");
        }

        [Theory]
        [InlineData("9003114476083")]
        [InlineData("9003195076083")]
        public void Validate_ValidIdNumbers_ShouldNotThrow(string idNumber)
        {
            // Act
            Action act = () => _identityValidation.Validate(idNumber);

            // Assert
            act.Should().NotThrow();
        }
    }
}
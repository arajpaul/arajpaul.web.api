﻿using MassTransit.Topology;

namespace Web.Api.RabbitMq.Helpers
{
    public class CustomExchangeNameFormatter : IEntityNameFormatter
    {
        private readonly string _exchangeName;

        public CustomExchangeNameFormatter(string exchangeName)
        {
            _exchangeName = exchangeName;
        }

        // Used to rename the exchanges
        public string FormatEntityName<T>()
        {
            return _exchangeName;
        }
    }
}

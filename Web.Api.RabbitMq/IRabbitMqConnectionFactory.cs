﻿namespace Web.Api.RabbitMq
{
    public interface IRabbitMqConnectionFactory<out T>
    {
        public T GetOrCreateBus(string busName);
    }
}

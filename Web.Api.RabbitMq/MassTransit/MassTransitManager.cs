﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using Web.Api.RabbitMq.Models;

namespace Web.Api.RabbitMq.MassTransit
{
    public class MassTransitManager : IRabbitMqManager
    {
        private readonly ILogger<MassTransitManager> _logger;
        private readonly IBus _bus;
        public readonly RabbitMqSettings RabbitMqSettings;

        public MassTransitManager(ILogger<MassTransitManager> logger, IConfiguration configuration, IBus bus)
        {
            _logger = logger;
            _bus = bus;
            RabbitMqSettings = configuration.GetSection("RabbitMq").Get<RabbitMqSettings>();
        }

        public bool AddToQueue<T>(T obj, string busName) where T : class, IQueueMessage
        {
            _bus.Publish(obj);
            return true;
        }

        public void SubscribeToQueue<T>(string busName, Action<T> action) where T : class, IQueueMessage
        {
            throw new NotImplementedException();
        }
    }
}

﻿using MassTransit;
using System;

namespace Web.Api.RabbitMq.MassTransit
{
    public interface IBusConfigurator
    {
        IBusControl ConfigureBus(IServiceProvider provider);
    }
}
﻿using MassTransit;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Web.Api.Base.Extensions;
using Web.Api.RabbitMq.Handlers;
using Web.Api.RabbitMq.Models;

namespace Web.Api.RabbitMq.MassTransit.Consumers
{
    public class OrderConsumer : IConsumer<OrderMessage>
    {
        private readonly ILogger<OrderConsumer> _logger;
        private readonly RabbitMqMessageHandler _queueProcessorHandler;

        public OrderConsumer(ILogger<OrderConsumer> logger, RabbitMqMessageHandler queueProcessorHandler)
        {
            _logger = logger;
            _queueProcessorHandler = queueProcessorHandler;
        }
        
        public async Task Consume(ConsumeContext<OrderMessage> context)
        {
            await Task.Run(() =>
            {
                try
                {
                    _queueProcessorHandler.Handle(context.Message);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Failed to process received message :: {context.Message.ToJson()}");
                }
            });
        }
    }
}

﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using Web.Api.RabbitMq.MassTransit.Consumers;
using Web.Api.RabbitMq.Models;

namespace Web.Api.RabbitMq.MassTransit
{
    public class BusConfigurator : IBusConfigurator
    {
        private readonly ILogger<BusConfigurator> _logger;
        private readonly RabbitMqSettings _rabbitMqSettings;

        public BusConfigurator(ILogger<BusConfigurator> logger, IConfiguration configuration)
        {
            _logger = logger;
            _rabbitMqSettings = configuration.GetSection("RabbitMq").Get<RabbitMqSettings>() ?? throw new Exception("The RabbitMq section in AppSettings is not set.");
        }

        public IBusControl ConfigureBus(IServiceProvider provider)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host(
                    host: _rabbitMqSettings.HostName,
                    port: _rabbitMqSettings.HostPort,
                    virtualHost: _rabbitMqSettings.VirtualHost,
                    configure: h =>
                    {
                        h.Username(_rabbitMqSettings.Username);
                        h.Password(_rabbitMqSettings.Password);
                    }
                );

                cfg.Send<OrderMessage>(x =>
                {
                    x.UseRoutingKeyFormatter(context => _rabbitMqSettings.RoutingKey);
                });
                cfg.Message<OrderMessage>(x =>
                {
                    x.SetEntityName(_rabbitMqSettings.ExchangeName);
                });
                cfg.Publish<OrderMessage>(x =>
                {
                    x.ExchangeType = ExchangeType.Topic;
                    x.Durable = true;
                    x.AutoDelete = false;
                });

                cfg.ReceiveEndpoint(_rabbitMqSettings.QueueName, ep =>
                {
                    ep.ConfigureConsumeTopology = false;
                    ep.Consumer<OrderConsumer>(provider);
                    ep.PrefetchCount = 1;
                    ep.BindQueue = true;

                    ep.Bind(_rabbitMqSettings.ExchangeName, s =>
                    {
                        s.RoutingKey = _rabbitMqSettings.RoutingKey;
                        s.ExchangeType = ExchangeType.Topic;
                        s.Durable = true;
                        s.AutoDelete = false;
                    });
                });
            });

            return bus;
        }
    }
}

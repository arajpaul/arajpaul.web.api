﻿using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using Web.Api.RabbitMq.MassTransit;
using Web.Api.RabbitMq.MassTransit.Consumers;

namespace Web.Api.RabbitMq.Extensions
{
    public static class RabbitMqExtensions
    {
        /// <summary>
        /// Add all dependent services for RabbitMq using RabbitMQ.Client
        /// </summary>
        public static void AddRabbitMqClient(this IServiceCollection services)
        {
            services.AddSingleton<IRabbitMqConnectionFactory<IModel>, RabbitMqClientConnectionFactory>();
            services.AddTransient<IRabbitMqManager, RabbitMqClientManager>();
        }

        /// <summary>
        /// Add all dependent services for RabbitMq using MassTransit
        /// </summary>
        public static void AddRabbitMqMassTransit(this IServiceCollection services)
        {
            services.AddSingleton<IBusConfigurator, BusConfigurator>();
            services.AddMassTransit(c =>
            {
                c.AddConsumer<OrderConsumer>();
                c.AddBus(provider =>
                {
                    var orderBus = provider.GetService<IBusConfigurator>();
                    return orderBus.ConfigureBus(provider);
                });
            });
            services.AddTransient<IRabbitMqManager, MassTransitManager>();

            //services.AddMassTransit(c =>
            //{
            //    c.AddConsumer<OrderConsumer>();
            //    c.UsingRabbitMq((context, cfg) =>
            //    {
            //        var orderBus = context.GetService<IBusConfigurator>();
            //        orderBus.ConfigureBus(context);
            //    });
            //});
        }
    }
}

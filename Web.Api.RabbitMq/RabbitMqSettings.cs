﻿namespace Web.Api.RabbitMq
{
    public class RabbitMqSettings
    {
        public string HostName { get; set; }
        public ushort HostPort { get; set; } = 5672;
        public string VirtualHost { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ExchangeName { get; set; }
        public string RoutingKey { get; set; }
        public string QueueName { get; set; }
        public string BusName { get; set; }
        public bool UseMassTransit { get; set; }
    }
}

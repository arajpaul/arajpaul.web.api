﻿using Microsoft.Extensions.Logging;
using System;
using Web.Api.Base.Extensions;
using Web.Api.Base.MessageBus;
using Web.Api.RabbitMq.Models;

namespace Web.Api.RabbitMq.Handlers
{
    public class RabbitMqMessageHandler : IMessageHandler<IQueueMessage>
    {
        private readonly ILogger<RabbitMqMessageHandler> _logger;

        public RabbitMqMessageHandler(ILogger<RabbitMqMessageHandler> logger)
        {
            _logger = logger;
        }

        public void Handle(IQueueMessage message)
        {
            message.UtcDateAckd = DateTime.UtcNow;
            _logger.LogInformation($"RabbitMQ Message received from queue :: {message.ToJson()}");
        }
    }
}

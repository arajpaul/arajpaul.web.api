﻿using System;
using Web.Api.RabbitMq.Models;

namespace Web.Api.RabbitMq
{
    public interface IRabbitMqManager
    {
        /// <summary>
        /// Add to specific RabbitMq queue using the <paramref name="busName"/> as the queue name
        /// </summary>
        public bool AddToQueue<T>(T obj, string busName) where T : class, IQueueMessage;

        public void SubscribeToQueue<T>(string busName, Action<T> action) where T : class, IQueueMessage;
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace Web.Api.RabbitMq
{
    public class RabbitMqClientConnectionFactory : IRabbitMqConnectionFactory<IModel>
    {
        private readonly ILogger<RabbitMqClientConnectionFactory> _logger;
        private readonly RabbitMqSettings _rabbitMqSettings;

        public readonly Dictionary<string, IConnection> Connections = new Dictionary<string, IConnection>();
        public readonly Dictionary<string, IModel> Buses = new Dictionary<string, IModel>();

        public RabbitMqClientConnectionFactory(ILogger<RabbitMqClientConnectionFactory> logger, IConfiguration configuration)
        {
            _logger = logger;
            _rabbitMqSettings = configuration.GetSection("RabbitMq").Get<RabbitMqSettings>() ?? throw new Exception("The RabbitMq section in AppSettings is not set.");
        }

        public IModel GetOrCreateBus(string busName)
        {
            if (Buses.TryGetValue(busName, out var bus) && bus.IsOpen)
            {
                _logger.LogDebug($"RabbitMq Host '{_rabbitMqSettings.HostName}' already connected.");
                return bus;
            }

            // Dispose if exists and not connected
            bus?.Dispose();

            return Buses[busName] = CreateBus();
        }

        /// <summary>
        /// Gets/Creates a default bus using the RabbitMq section from AppSettings
        /// </summary>
        /// <returns></returns>
        private IConnection GetOrCreateConnection()
        {
            if (Connections.TryGetValue(_rabbitMqSettings.HostName, out var conn) && conn.IsOpen)
            {
                _logger.LogDebug($"RabbitMq Host '{_rabbitMqSettings.HostName}' already connected.");
                return conn;
            }

            // Dispose if exists and not connected
            conn?.Dispose();

            return Connections[_rabbitMqSettings.HostName] = new ConnectionFactory
            {
                HostName = _rabbitMqSettings.HostName,
                UserName = _rabbitMqSettings.Username,
                Password = _rabbitMqSettings.Password,
                VirtualHost = _rabbitMqSettings.VirtualHost,
            }.CreateConnection();
        }

        private IModel CreateBus()
        {
            var connection = GetOrCreateConnection();
            var bus = connection.CreateModel();
            return bus;
        }
    }
}

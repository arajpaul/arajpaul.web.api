﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using Web.Api.Base.Extensions;
using Web.Api.RabbitMq.Models;

namespace Web.Api.RabbitMq
{
    public class RabbitMqClientManager : IRabbitMqManager
    {
        private readonly ILogger<RabbitMqClientManager> _logger;
        private readonly IRabbitMqConnectionFactory<IModel> _rabbitMqConnectionFactory;
        public readonly RabbitMqSettings RabbitMqSettings;

        public RabbitMqClientManager(ILogger<RabbitMqClientManager> logger, IConfiguration configuration, IRabbitMqConnectionFactory<IModel> rabbitMqConnectionFactory)
        {
            _logger = logger;
            _rabbitMqConnectionFactory = rabbitMqConnectionFactory;
            RabbitMqSettings = configuration.GetSection("RabbitMq").Get<RabbitMqSettings>();
        }

        /// <summary>
        /// Add to specific RabbitMq queue using the <paramref name="busName"/> as the queue name
        /// </summary>
        public bool AddToQueue<T>(T obj, string busName) where T : class, IQueueMessage
        {
            try
            {
                var bus = _rabbitMqConnectionFactory.GetOrCreateBus(busName);
                SetupExchangeQueue(bus, RabbitMqSettings.ExchangeName, RabbitMqSettings.QueueName, RabbitMqSettings.RoutingKey);

                obj.UtcDateAdded = DateTime.UtcNow;

                // Publish to queue
                bus.BasicPublish(RabbitMqSettings.ExchangeName, RabbitMqSettings.RoutingKey, null, Encoding.UTF8.GetBytes(obj.ToJson()));

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Failed to publish to RabbitMq Host '{RabbitMqSettings.HostName}:{RabbitMqSettings.HostPort}', VHost '{RabbitMqSettings.VirtualHost}', Exchange '{RabbitMqSettings.ExchangeName}' with Routing Key '{RabbitMqSettings.RoutingKey}'.");
                return false;
            }
        }

        private static void SetupExchangeQueue(IModel bus, string exchangeName, string queueName, string routingKey)
        {
            // Get/Create the exchange if it doesn't exist
            bus.ExchangeDeclare(exchangeName, ExchangeType.Topic, true);

            // Get/Create the queue if it doesn't exist
            var queue = bus.QueueDeclare(queueName, true, false, false);

            // Bind the exchange and the queue together
            bus.QueueBind(queue.QueueName, exchangeName, routingKey);
        }

        public void SubscribeToQueue<T>(string busName, Action<T> action) where T : class, IQueueMessage
        {
            try
            {
                var bus = _rabbitMqConnectionFactory.GetOrCreateBus(busName);
                SetupExchangeQueue(bus, RabbitMqSettings.ExchangeName, RabbitMqSettings.QueueName, RabbitMqSettings.RoutingKey);

                var consumer = new EventingBasicConsumer(bus);
                consumer.Received += (model, ea) =>
                {
                    try
                    {
                        var jsonMsg = Encoding.UTF8.GetString(ea.Body.ToArray());
                        var body = jsonMsg.FromJson<T>();
                        body.UtcDateAckd = DateTime.UtcNow;
                        _logger.LogInformation($"Message received from queue :: {body.ToJson()}");
                        action(body);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Failed to process received message :: {Encoding.UTF8.GetString(ea.Body.ToArray())}");
                    }
                };
                bus.BasicConsume(RabbitMqSettings.QueueName, true, consumer);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Failed to subscribe to RabbitMq default Host '{RabbitMqSettings.HostName}:{RabbitMqSettings.HostPort}', VHost '{RabbitMqSettings.VirtualHost}' on Queue '{RabbitMqSettings.QueueName}'.");
            }
        }
    }
}

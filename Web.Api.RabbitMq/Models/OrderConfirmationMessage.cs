﻿namespace Web.Api.RabbitMq.Models
{
    public class OrderConfirmationMessage
    {
        public int OrderNo { get; set; }

        public string Type { get; set; }
    }
}

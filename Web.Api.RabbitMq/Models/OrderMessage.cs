﻿using System;

namespace Web.Api.RabbitMq.Models
{
    public class OrderMessage : IQueueMessage
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime UtcDateAdded { get; set; }
        public DateTime? UtcDateAckd { get; set; }
    }
}

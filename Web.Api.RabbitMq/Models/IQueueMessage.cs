﻿using System;

namespace Web.Api.RabbitMq.Models
{
    public interface IQueueMessage
    {
        public DateTime UtcDateAdded { get; set; }
        public DateTime? UtcDateAckd { get; set; }
    }
}

﻿namespace Web.Api.Logic.Validations
{
    public interface IIdentityValidation
    {
        void Validate(string idNumber);
    }
}
﻿using System;

namespace Web.Api.Logic.Validations
{
    /// <summary>
    /// Performs validation of a South African Identity Number
    /// </summary>
    public class IdentityValidation : BaseValidationService, IIdentityValidation
    {

        public IdentityValidation() : base()
        {
        }

        /// <summary>
        /// Perform validation of input
        /// </summary>
        public void Validate(string idNumber)
        {
            ValidateModel(idNumber);
            ValidateBusinessRules(idNumber);
        }

        private void ValidateModel(string idNumber)
        {
            if (string.IsNullOrWhiteSpace(idNumber))
                throw new Exception("Please provide an ID Number to validate.");
        }

        private void ValidateBusinessRules(string idNumber)
        {
            if (idNumber.Length != 13)
                throw new Exception("The provided ID Number is invalid.");


            // 0 - SA Citizen
            // 1 - Permanent SA resident
            var citizenship = idNumber.Substring(10, 1);
            var valid = idNumber.Length == 13
                        && (citizenship == "0" || citizenship == "1")
                        && CheckLuhnDigit(idNumber);
            if (!valid)
                throw new Exception("The provided ID Number is invalid.");
        }

        /// <summary>
        /// Verifies if the CheckSum digit in a South African ID number (Last Digit) is correct
        /// </summary>
        private static bool CheckLuhnDigit(string identityNumber)
        {
            var nDigits = identityNumber.Length;

            var nSum = 0;
            var isSecond = false;
            for (var i = nDigits - 1; i >= 0; i--)
            {

                var d = identityNumber[i] - '0';

                if (isSecond)
                    d = d * 2;

                // We add two digits to handle
                // cases that make two digits 
                // after doubling
                nSum += d / 10;
                nSum += d % 10;

                isSecond = !isSecond;
            }
            return nSum % 10 == 0;
        }
    }
}

﻿namespace Web.Api.Kafka
{
    public class AppSettings
    {
        public string BootstrapServers { get; set; }
        public string Topic { get; set; }

        public AppSettings() { }

        /// <summary>
        /// Static instance for use in static classes which is bound in Startup
        /// </summary>
        public static AppSettings Instance { get; protected set; } = new AppSettings();
    }
}

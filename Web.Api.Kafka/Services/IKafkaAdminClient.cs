﻿using System.Threading.Tasks;

namespace Web.Api.Kafka.Services
{
    public interface IKafkaAdminClient
    {
        Task CreateTopic(string bootstrapServers, string topic, short replicationFactor, int numberOfPartitions);
    }
}
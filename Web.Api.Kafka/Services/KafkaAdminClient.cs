﻿using Confluent.Kafka;
using Confluent.Kafka.Admin;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Web.Api.Kafka.Services
{
    public class KafkaAdminClient : IKafkaAdminClient
    {
        private readonly ILogger<KafkaAdminClient> _logger;

        public KafkaAdminClient(ILogger<KafkaAdminClient> logger)
        {
            _logger = logger;
        }

        public async Task CreateTopic(string bootstrapServers, string topic, short replicationFactor = 1, int numberOfPartitions = 1)
        {
            using var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = bootstrapServers }).Build();
            try
            {
                await adminClient.CreateTopicsAsync(new[] {
                    new TopicSpecification
                    {
                        Name = topic, ReplicationFactor = replicationFactor, NumPartitions = numberOfPartitions
                    },
                });
            }
            catch (CreateTopicsException e)
            {
                _logger.LogInformation($"An error occurred while creating topic `{e.Results[0].Topic}` :: {e.Results[0].Error.Reason}");
            }
        }
    }
}

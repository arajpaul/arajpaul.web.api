﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Web.Api.Kafka.Services
{
    public interface IKafkaConsumer<TKey, out TValue> : IDisposable
    {
        Task Consume(string topic, Action<TValue> action, CancellationToken cancellationToken = default);
    }
}
﻿using Confluent.Kafka;
using Google.Protobuf;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Web.Api.Kafka.Services
{
    public class KafkaProtoProducer<TKey, TValue> : IKafkaProducer<TKey, TValue> where TValue : IMessage<TValue>, new()
    {
        private readonly ILogger<KafkaProtoProducer<TKey, TValue>> _logger;
        private readonly IProducer<TKey, TValue> _producer;

        public KafkaProtoProducer(ILogger<KafkaProtoProducer<TKey, TValue>> logger, IKafkaClientHandle kafkaClientHandle)
        {
            _logger = logger;

            // If you want to produce different types of messages to the same Kafka instance in ProducerConfig
            // should only be talking to one instance
            _producer = new DependentProducerBuilder<TKey, TValue>(kafkaClientHandle.GetHandle())
                .SetValueSerializer(new ProtoSerializer<TValue>())
                .Build();


            // If you want to produce different types of messages to different Kafka instances
            //_producer = new ProducerBuilder<TKey, TValue>(_producerConfig)
            //    .SetValueSerializer(new ProtoSerializer<TValue>())
            //    .SetErrorHandler((_, e) => Console.WriteLine($"Producer Error: {e.Reason}"))
            //    .Build();
        }

        /// <summary>
        ///     Asynchronously produce a message and expose delivery information
        ///     via the returned Task. Use this method of producing if you would
        ///     like to await the result before flow of execution continues.
        /// </summary>
        public Task ProduceAsync(string topic, TValue message)
        {
            try
            {
                // Note: Awaiting the asynchronous produce request below prevents flow of execution
                // from proceeding until the acknowledgement from the broker is received (at the 
                // expense of low throughput).
                // https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/examples/ExactlyOnce/Program.cs#:~:text=//%20Note%3A%20producing%20synchronously%20is%20slow%20and%20should%20generally%20be%20avoided.
                return _producer.ProduceAsync(topic, new Message<TKey, TValue>
                {
                    Value = message,
                    Timestamp = Timestamp.Default,
                });
            }
            catch (ProduceException<TKey, TValue> ex)
            {
                _logger.LogError($"Failed to deliver message :: {ex} [{ex.Error.Code}]");
                return Task.FromException(ex);
;            }
        }

        /// <summary>
        ///     Asynchronously produce a message and expose delivery information
        ///     via the provided callback function. Use this method of producing
        ///     if you would like flow of execution to continue immediately, and
        ///     handle delivery information out-of-band.
        /// </summary>
        public void Produce(string topic, TValue message, Action<DeliveryReport<TKey, TValue>> deliveryHandler = null)
        {
            try
            {
                _producer.Produce(topic, new Message<TKey, TValue>
                {
                    Value = message,
                    Timestamp = Timestamp.Default,
                }, deliveryHandler);
            }
            catch (ProduceException<TKey, TValue> e)
            {
                _logger.LogError($"Failed to deliver message :: {e} [{e.Error.Code}]");
            }
        }

        public void Dispose()
        {
            // Block until all outstanding produce requests have completed (with or
            // without error).
            _producer.Flush();
            _producer.Dispose();
        }
    }
}

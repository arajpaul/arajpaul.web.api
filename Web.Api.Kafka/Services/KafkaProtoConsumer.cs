﻿using Confluent.Kafka;
using Google.Protobuf;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Web.Api.Kafka.Services
{
    public class KafkaProtoConsumer<TKey, TValue> : IKafkaConsumer<TKey, TValue> where TValue : IMessage<TValue>, new()
    {
        private readonly ILogger<KafkaProtoConsumer<TKey, TValue>> _logger;
        private readonly IConsumer<TKey, TValue> _consumer;

        public KafkaProtoConsumer(ILogger<KafkaProtoConsumer<TKey, TValue>> logger, IConfiguration config)
        {
            _logger = logger;

            var consumerConfig = new ConsumerConfig();
            config.GetSection("Kafka:ConsumerConfig").Bind(consumerConfig);
            consumerConfig.ClientId = Dns.GetHostName();

            _consumer = new ConsumerBuilder<TKey, TValue>(consumerConfig)
                    .SetValueDeserializer(new ProtoDeserializer<TValue>())
                    .SetErrorHandler((_, e) => Console.WriteLine($"Consumer Error: {e.Reason}"))
                    .Build();
        }

        public async Task Consume(string topic, Action<TValue> action, CancellationToken cancellationToken = default)
        {
            // Subscribe to topic
            _consumer.Subscribe(topic);

            // Poll for new messages on a new thread
            await Task.Run(() =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        var cr = _consumer.Consume(cancellationToken);
                        action(cr.Message.Value);
                    }
                    catch (OperationCanceledException)
                    {
                        _logger.LogError("Connection closed due to app cancellation");
                        break;
                    }
                    catch (ConsumeException e)
                    {
                        // Consumer errors should generally be ignored (or logged) unless fatal.
                        Console.WriteLine($"Failed to process consumed message :: {e.Error.Reason}");

                        if (e.Error.IsFatal)
                        {
                            // https://github.com/edenhill/librdkafka/blob/master/INTRODUCTION.md#fatal-consumer-errors
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Unexpected error: {e}");
                        break;
                    }
                }
            }, cancellationToken);
        }

        public void Dispose()
        {
            // Clean up
            _consumer.Close();
            _consumer.Dispose();
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace Web.Api.Kafka.Services
{
    public interface IKafkaProducer<TKey, TValue> : IDisposable
    {
        Task ProduceAsync(string topic, TValue message);
        void Produce(string topic, TValue message, Action<DeliveryReport<TKey, TValue>> deliveryHandler = null);
    }
}
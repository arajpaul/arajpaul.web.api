﻿using System;
using Confluent.Kafka;

namespace Web.Api.Kafka.Services
{
    public interface IKafkaClientHandle : IDisposable
    {
        Handle GetHandle();
    }
}
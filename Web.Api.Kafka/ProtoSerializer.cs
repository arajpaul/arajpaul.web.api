﻿using Confluent.Kafka;
using Google.Protobuf;

namespace Web.Api.Kafka
{
    public class ProtoSerializer<T> : ISerializer<T> where T : IMessage<T>
    {
        public byte[] Serialize(T data, SerializationContext context)
        {
            return data.ToByteArray();
        }
    }
}

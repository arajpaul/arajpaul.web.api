﻿using System;
using Web.Api.Base.Extensions;
using Web.Api.Base.MessageBus;

namespace Web.Api.Kafka.Handlers
{
    public class OrderConfirmationMessageHandler : IMessageHandler<ProtobufOrderConfirmationMessage>
    {
        public void Handle(ProtobufOrderConfirmationMessage message)
        {
            Console.WriteLine($"Order Confirmation Message received :: {message.ToJson()}");
        }
    }
}
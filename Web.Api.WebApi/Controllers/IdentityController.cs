﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Web.Api.Logic.Validations;

namespace Web.Api.WebApi.Controllers
{
    public class IdentityController : BaseController<IdentityController>
    {
        private readonly IIdentityValidation _identityValidation;

        public IdentityController(ILogger<IdentityController> logger, IIdentityValidation identityValidation) : base(logger)
        {
            _identityValidation = identityValidation;
        }

        [HttpGet]
        public IActionResult ValidateIdNumber(string idNumber)
        {
            try
            {
                _identityValidation.Validate(idNumber);
                return Ok(new {Message = "The provided ID Number is valid."});
            }
            catch (Exception ex)
            {
                return ThrowJsonError(ex);
            }
        }
    }
}
﻿using Confluent.Kafka;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Web.Api.Kafka;
using Web.Api.Kafka.Services;
using Web.Api.RabbitMq;
using Web.Api.RabbitMq.Models;
using Timestamp = Google.Protobuf.WellKnownTypes.Timestamp;

namespace Web.Api.WebApi.Controllers
{
    public class EventBusController : BaseController<EventBusController>
    {
        private readonly IRabbitMqManager _rabbitMqManager;
        private readonly IKafkaAdminClient _kafkaAdminClient;
        private readonly IKafkaProducer<Null, ProtobufOrderConfirmationMessage> _kafkaProducer;
        private readonly RabbitMqSettings _rabbitMqSettings;

        private const string SuccessfullyPublishedToQueueMessage = "Successfully published.";
        private const string FailedToPublishToQueueMessage = "An unknown error occurred while trying to publish.";

        public EventBusController(
            ILogger<EventBusController> logger, 
            IConfiguration configuration, 
            IRabbitMqManager rabbitMqManager, 
            IKafkaAdminClient kafkaAdminClient,
            IKafkaProducer<Null, ProtobufOrderConfirmationMessage> kafkaProducer) : base(logger)
        {
            _rabbitMqSettings = configuration.GetSection("RabbitMq").Get<RabbitMqSettings>() ?? throw new Exception("The RabbitMq section in AppSettings is not set.");
            _rabbitMqManager = rabbitMqManager;
            _kafkaAdminClient = kafkaAdminClient;
            _kafkaProducer = kafkaProducer;
        }

        [HttpPost]
        public async Task<IActionResult> PublishRabbitMq(OrderMessage body)
        {
            try
            {
                return await Task.Run(() => _rabbitMqManager.AddToQueue(body, _rabbitMqSettings.BusName))
                    ? Ok(new { Message = SuccessfullyPublishedToQueueMessage })
                    : throw new Exception(FailedToPublishToQueueMessage);
            }
            catch (Exception ex)
            {
                return ThrowJsonError(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> PublishKafka(OrderConfirmationMessage body)
        {
            try
            {
                var protoMessage = new ProtobufOrderConfirmationMessage
                {
                    OrderNo = body.OrderNo,
                    CommunicationType = body.Type,
                    UtcDateTime = new Timestamp(),
                };

                await _kafkaProducer.ProduceAsync("new_message", protoMessage);
                return Ok(new { Message = SuccessfullyPublishedToQueueMessage });
            }
            catch (Exception ex)
            {
                return ThrowJsonError(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateKafkaTopic(string topic)
        {
            try
            {
                await _kafkaAdminClient.CreateTopic("127.0.0.1:9093", topic, 1, 1);
                return Ok(new { Message = "Topic created." });
            }
            catch (Exception ex)
            {
                return ThrowJsonError(ex);
            }
        }
    }
}
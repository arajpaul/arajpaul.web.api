﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Web.Api.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseController<T> : ControllerBase
    {
        protected readonly ILogger<T> Logger;

        public BaseController(ILogger<T> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Converts exception into a JSON format suitable for getting an error message through for display
        /// </summary>
        protected JsonResult ThrowJsonError(Exception ex)
        {
            var message = ex.Message;
            return ThrowJsonError(message);
        }

        /// <summary>
        /// Converts exception into a JSON format suitable for getting an error message through for display
        /// </summary>
        protected JsonResult ThrowJsonError(string message)
        {
            Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            return new JsonResult(new { Message = message });
        }
    }
}

﻿using Web.Api.RabbitMq;

namespace Web.Api.WebApi
{
    public class AppSettings
    {
        public RabbitMqSettings RabbitMq { get; set; }

        public AppSettings() { }

        /// <summary>
        /// Static instance for use in static classes which is bound in Startup
        /// </summary>
        public static AppSettings Instance { get; protected set; } = new AppSettings();
    }
}

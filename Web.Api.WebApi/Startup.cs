using Confluent.Kafka;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Web.Api.Kafka;
using Web.Api.Kafka.Services;
using Web.Api.Logic.Validations;
using Web.Api.RabbitMq.Extensions;
using Web.Api.RabbitMq.Handlers;

namespace Web.Api.WebApi
{
    public class Startup
    {
        public AppSettings AppSettings { get; }

        public Startup(IConfiguration configuration)
        {
            AppSettings = configuration.Get<AppSettings>();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Akshay Rajpaul - Web API",
                    Version = "v1"
                });
            });

            // RabbitMq Manager
            if (AppSettings.RabbitMq.UseMassTransit)
            {
                services.AddRabbitMqMassTransit();
            }
            else
            {
                services.AddRabbitMqClient();
            }

            services.AddTransient<IKafkaAdminClient, KafkaAdminClient>();
            services.AddSingleton<IKafkaClientHandle, KafkaClientHandle>();
            services.AddSingleton<IKafkaProducer<Null, ProtobufOrderConfirmationMessage>, KafkaProtoProducer<Null, ProtobufOrderConfirmationMessage>>();

            // Validation services
            services.AddTransient<IIdentityValidation, IdentityValidation>();

            services.AddTransient<RabbitMqMessageHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("../swagger/v1/swagger.json", "My API v1");
                    c.RoutePrefix = "";
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

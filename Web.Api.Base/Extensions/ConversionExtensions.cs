﻿using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace Web.Api.Base.Extensions
{
    public static class ConversionExtensions
    {

        /// <summary>
        /// Converts a Json string into an object
        /// </summary>
        public static T FromJson<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Converts an object into a Json string
        /// </summary>
        public static string ToJson<T>(this T value, bool indented = false, bool ignoreNulls = false)
        {
            var settings = new JsonSerializerSettings
            {
                Formatting = indented ? Formatting.Indented : Formatting.None,
                NullValueHandling = ignoreNulls ? NullValueHandling.Ignore : NullValueHandling.Include
            };
            return JsonConvert.SerializeObject(value, settings);
        }
    }
}

﻿namespace Web.Api.Base.MessageBus
{
    public interface IMessageHandler<in TMessage>
    {
        void Handle(TMessage message);
    }
}
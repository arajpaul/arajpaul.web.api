﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Runtime.InteropServices;
using Confluent.Kafka;
using Web.Api.Base.MessageBus;
using Web.Api.Kafka;
using Web.Api.Kafka.Handlers;
using Web.Api.Kafka.Services;
using Web.Api.RabbitMq.Extensions;
using Web.Api.RabbitMq.Handlers;
using Web.Api.RabbitMq.Models;

namespace Web.Api.QueueProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            var hostBuilder = CreateHostBuilder(args);
            LoadConfiguration(hostBuilder);
            ConfigureLogging(hostBuilder);
            ConfigureServices(hostBuilder);

            var host = hostBuilder.Build();

            host.Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args);

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                hostBuilder.UseWindowsService();
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                hostBuilder.UseSystemd();
            }

            return hostBuilder;
        }

        public static void LoadConfiguration(IHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureAppConfiguration((hostBuilderContext, config) =>
            {
                config.AddJsonFile($"appsettings.{hostBuilderContext.HostingEnvironment.EnvironmentName.ToLowerInvariant()}.json", true, true);
                config.AddEnvironmentVariables();

                hostBuilder.ConfigureServices(services =>
                {
                    services.Configure<AppSettings>(hostBuilderContext.Configuration);
                });
            });
        }

        private static void ConfigureLogging(IHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureLogging((hostBuilderContext, logging) =>
            {
            });
        }

        private static void ConfigureServices(IHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureServices((hostBuilderContext, services) =>
            {
                var appSettings = hostBuilderContext.Configuration.Get<AppSettings>();

                // RabbitMq Manager
                if (appSettings.RabbitMq.UseMassTransit)
                {
                    services.AddRabbitMqMassTransit();
                }
                else
                {
                    services.AddRabbitMqClient();
                }

                services.AddTransient<IMessageHandler<IQueueMessage>, RabbitMqMessageHandler>();

                // Kafka
                services.AddTransient<IKafkaAdminClient, KafkaAdminClient>();

                services.AddSingleton<IKafkaConsumer<Null, ProtobufOrderConfirmationMessage>, KafkaProtoConsumer<Null, ProtobufOrderConfirmationMessage>>();
                services.AddTransient<IMessageHandler<ProtobufOrderConfirmationMessage>, OrderConfirmationMessageHandler>();
                
                // Main
                services.AddHostedService<Services.QueueProcessor>();
            });
        }
    }
}
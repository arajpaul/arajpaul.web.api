﻿using Confluent.Kafka;
using MassTransit;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;
using Web.Api.Base.MessageBus;
using Web.Api.Kafka;
using Web.Api.Kafka.Services;
using Web.Api.RabbitMq;
using Web.Api.RabbitMq.Models;

namespace Web.Api.QueueProcessor.Services
{
    public class QueueProcessor : BackgroundService
    {
        private static ILogger _logger;
        private readonly IRabbitMqManager _rabbitMqManager;
        private readonly IMessageHandler<IQueueMessage> _rabbitMessageHandler;
        private readonly IKafkaConsumer<Null, ProtobufOrderConfirmationMessage> _kafkaConsumer;
        private readonly IMessageHandler<ProtobufOrderConfirmationMessage> _kafkaOrderConfirmationMessageHandler;
        private readonly IServiceProvider _serviceProvider;
        private readonly AppSettings _appSettings;

        public QueueProcessor(
            ILogger<QueueProcessor> logger,
            IOptions<AppSettings> appSettings,
            IRabbitMqManager rabbitMqManager,
            IMessageHandler<IQueueMessage> rabbitMessageHandler,
            IKafkaConsumer<Null, ProtobufOrderConfirmationMessage> kafkaConsumer,
            IMessageHandler<ProtobufOrderConfirmationMessage> kafkaOrderConfirmationMessageHandler,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            _rabbitMqManager = rabbitMqManager;
            _rabbitMessageHandler = rabbitMessageHandler;
            _kafkaConsumer = kafkaConsumer;
            _kafkaOrderConfirmationMessageHandler = kafkaOrderConfirmationMessageHandler;
            _serviceProvider = serviceProvider;
            _appSettings = appSettings.Value;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Application started");

            // RabbitMQ
            if (_appSettings.RabbitMq.UseMassTransit)
            {
                _logger.LogInformation("Using MassTransit.");
                var bus = _serviceProvider.GetService(typeof(IBusControl)) as IBusControl;
                if (bus != null)
                    _ = bus.StartAsync(cancellationToken);
            }
            else
            {
                _logger.LogInformation("Using RabbitMQ.Client.");
                _ = Task.Run(() => _rabbitMqManager.SubscribeToQueue<OrderMessage>(_appSettings.RabbitMq.BusName, _rabbitMessageHandler.Handle), cancellationToken);
            }

            // Kafka
            _ = _kafkaConsumer.Consume("new_message", _kafkaOrderConfirmationMessageHandler.Handle, cancellationToken);

            await Task.Run(Console.ReadLine, cancellationToken);
        }
    }
}
